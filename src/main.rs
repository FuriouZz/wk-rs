mod importer;
mod task;
mod utils;
mod error;

fn main() {
  let args: Vec<String> = std::env::args().collect();
  println!("{:#?}", args);
}

fn target_dir() -> std::path::PathBuf {
  std::env::current_exe()
      .ok()
      .map(|mut path| {
          path.pop();
          if path.ends_with("deps") {
              path.pop();
          }
          path
      })
      .unwrap()
}

#[cfg(test)]
mod tests {
  use std::path::{Path, PathBuf};
  use super::utils::path::*;
  use super::*;

  #[test]
  fn run() -> Result<(), Box<dyn std::error::Error>> {
    let name = env!("CARGO_PKG_NAME");
    println!("{:?}", name);
    println!("{:?}", target_dir());
    println!("{:?}", std::env::current_exe());
    println!("{:?}", std::env::consts::EXE_SUFFIX);
    println!("{:?}", std::env::var_os("CARGO_BIN_PATH"));
    println!("{:?}", target_dir().join(format!("{}{}", name, std::env::consts::EXE_SUFFIX)));
    // let args: Vec<String> = std::env::args().collect();
    // println!("{:#?}", args);

    // let res = importer::lookup_from_path("tmp")?;
    // println!("{:#?}", res.keys());
    Ok(())
  }

  #[test]
  fn lookup() {
    let res = importer::lookup();
    println!("{:#?}", res);
    let res = importer::lookup_from_path("tmp");
    println!("{:#?}", res);
  }

  #[test]
  fn parse_file() -> Result<(), Box<dyn std::error::Error>> {
    let path: PathBuf = Path::new("./")
      .join("tmp")
      .join("Commands.toml")
      .normalize();

    let tasks = importer::load(&path)?;
    println!("{:#?}", tasks);

    task::Task::new().with_name("Coucou").with_cwd(&path);

    let task: task::Task = "echo Hello World".parse().unwrap();
    println!("{:#?}", task);
    // // let result = Reader::toml_value(path)?;
    // println!("{:?}", result);

    // let command: CommandFile = toml::from_str(result.as_str()).unwrap();
    // println!("{:?}", command);

    Ok(())
  }
}
