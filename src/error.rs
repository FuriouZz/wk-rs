use std::collections::HashMap;

#[derive(Debug)]
pub enum WKError {
  NotFound,
  LookupFailed(HashMap<String, Box<dyn std::error::Error>>),
}

impl std::fmt::Display for WKError {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "An Error Occurred, Please Try Again!") // user-facing output
  }
}

impl std::error::Error for WKError {
  fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
      // Generic error, underlying cause isn't tracked.
      None
  }
}

pub type WKResult<T> = Result<T, WKError>;